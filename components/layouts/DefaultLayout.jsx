import React from 'react';
import { connect } from 'react-redux';
import Head from './modules/Head';
import {Container, Row, Col} from "react-bootstrap";

const DefaultLayout = ({ children }) => (
    <div className="layout--default">
        <Head />
        <Container >
            <Row>
                <Col >
                    {children}
                </Col>
            </Row>
        </Container>

    </div>
);

export default DefaultLayout;
