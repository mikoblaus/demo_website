import React, { Component } from 'react';
import Link from 'next/link';

import { signInCandidateInformation } from '../../../store/auth/action';
import {connect} from "react-redux";

class HeaderDefault extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (localStorage.getItem("token")) {
            this.props.dispatch(signInCandidateInformation())
        }
    }

    render() {
        return (
            <header >
                <h1>{this.props.pageName}</h1>
            </header>
        );
    }
}
const mapStateToProps = state => {
    return state.auth;
};
export default connect(mapStateToProps)(HeaderDefault);
