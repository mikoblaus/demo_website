import { all } from 'redux-saga/effects';
import App from './app/saga';
import AuthSaga from './auth/saga';
import SearchSaga from './search/saga';
import UsersSaga from './users/saga';
import ChatSaga from './chat/saga';

export default function* rootSaga() {
    yield all([
        App(),
        AuthSaga(),
        SearchSaga(),
        UsersSaga(),
        ChatSaga(),
    ]);
}
