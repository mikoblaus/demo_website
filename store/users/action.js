export const actionTypes = {
    ADD_USER_IN_CHATROOM: 'ADD_USER_IN_CHATROOM',
    ADD_USER_IN_CHATROOM_SUCCESS: 'ADD_USER_IN_CHATROOM_SUCCESS',
};

export function addUser(payload) {
    return { type: actionTypes.ADD_USER_IN_CHATROOM, payload };
}
export function addUserSuccess(payload) {
    return { type: actionTypes.ADD_USER_IN_CHATROOM_SUCCESS, payload };
}
