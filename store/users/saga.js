import {all, put, takeEvery, call} from 'redux-saga/effects';
import {Modal, notification} from 'antd';
import {actionTypes, addUserSuccess} from './action';
import { getContacts} from '../chat/action';
import Router from "next/router";
import Config from '../../utilities/app-settings'



function* AddUserInChatRoomSaga({payload}) {
    try {
        const callBack = yield call(AddUserInChatRoomSagaAsync, payload)
        if (callBack.success) {
            yield put(addUserSuccess(callBack))
            yield put(getContacts())
        }
    } catch (err) {
        console.log(err);
    }
}
const AddUserInChatRoomSagaAsync = async (stateForm) => {
    let token = localStorage.getItem("token")
    const response = await fetch(`${Config.URL_SERVER}/api/chat/addUser`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`,
        },
        body: JSON.stringify(stateForm)
    })
    return await response.json()
}


export default function* rootSaga() {
    yield all([takeEvery(actionTypes.ADD_USER_IN_CHATROOM, AddUserInChatRoomSaga)]);
}
