import {actionTypes} from './action';

export const initState = {
    contactsArray: [],
    messagesArray: [],
    messagesStatus : false
};

function reducer(state = initState, action) {
    switch (action.type) {
        case actionTypes.GET_CONTACTS_SUCCESS:
            return {
                ...state,
                ...{contactsArray: action.payload.result},
            };
        case actionTypes.GET_MESSAGES_SUCCESS:
            return {
                ...state,
                ...{messagesArray: action.payload.result,  messagesStatus : true},
            };
        case actionTypes.ADD_MESSAGES_SUCCESS:
            let tr = state.messagesArray
            let message = {
                chatRoomId: action.payload.chatRoomId,
                createdAt: action.payload.createdAt,
                message: action.payload.message,
                see: 0,
                userId: action.payload.userId,
                _id: action.payload._id
            }
            return {
                ...state,
                ...{messagesArray: [...tr, message]},
            };
        default:
            return state;
    }
}

export default reducer;
