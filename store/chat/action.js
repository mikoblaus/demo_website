export const actionTypes = {
    GET_CONTACTS: 'GET_CONTACTS',
    GET_CONTACTS_SUCCESS: 'GET_CONTACTS_SUCCESS',
    GET_MESSAGES: 'GET_MESSAGES',
    GET_MESSAGES_SUCCESS: 'GET_MESSAGES_SUCCESS',
    ADD_MESSAGES: 'ADD_MESSAGES',
    ADD_MESSAGES_SUCCESS: 'ADD_MESSAGES_SUCCESS',
};

export function getContacts(payload) {
    return { type: actionTypes.GET_CONTACTS, payload };
}

export function getContactsSuccess(payload) {
    return { type: actionTypes.GET_CONTACTS_SUCCESS, payload };
}


export function getMessages(payload) {
    return { type: actionTypes.GET_MESSAGES, payload };
}

export function getMessagesSuccess(payload) {
    return { type: actionTypes.GET_MESSAGES_SUCCESS, payload };
}



export function addMessages(payload) {
    return { type: actionTypes.ADD_MESSAGES, payload };
}

export function addMessagesSuccess(payload) {
    return { type: actionTypes.ADD_MESSAGES_SUCCESS, payload };
}

