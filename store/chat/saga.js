import {all, put, takeEvery, call} from 'redux-saga/effects';
import {Modal, notification} from 'antd';
import {actionTypes, getContactsSuccess, getMessagesSuccess} from './action';
import Router from "next/router";
import Config from '../../utilities/app-settings'

function* GetContactsSaga({payload}) {
    try {
        const callBack = yield call(GetContactsSagaAsync, payload)
        if (callBack.success) {
            yield put(getContactsSuccess(callBack))
        }
    } catch (err) {
        console.log(err);
    }
}

const GetContactsSagaAsync = async (stateForm) => {
    let token = localStorage.getItem("token")
    const response = await fetch(`${Config.URL_SERVER}/api/chat/chatrooms`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`,
        }
    })
    return await response.json()
}

function* GetMessagesSaga({payload}) {
    try {
        const callBack = yield call(GetMessagesSagaAsync, payload)
        if (callBack.success) {
            yield put(getMessagesSuccess(callBack))
        }
    } catch (err) {
        console.log(err);
    }
}

const GetMessagesSagaAsync = async (stateForm) => {
    let token = localStorage.getItem("token")
    const response = await fetch(`${Config.URL_SERVER}/api/chat/messages/${stateForm}`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`,
        }
    })
    return await response.json()
}


export default function* rootSaga() {
    yield all([takeEvery(actionTypes.GET_CONTACTS, GetContactsSaga)]);
    yield all([takeEvery(actionTypes.GET_MESSAGES, GetMessagesSaga)]);
}
