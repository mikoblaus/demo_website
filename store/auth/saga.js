import {all, put, takeEvery, call} from 'redux-saga/effects';
import {Modal, notification} from 'antd';
import {actionTypes, signInCandidateInformationSuccess} from './action';
import Router from "next/router";
import Config from '../../utilities/app-settings'

const modalSuccess = type => {
    notification[type]({
        message: 'Wellcome back',
        description: 'You are login successful!',
    });
};

const modalRegistrationSuccess = type => {
    notification[type]({
        message: 'Sign In',
        description: 'You have been registered successfully, Please log in',
    });
};

const modalWarning = type => {
    notification[type]({
        message: 'Warning!',
        description: 'This e-mail is already registered!',
    });
};
const modalSignInWarning = type => {
    notification[type]({
        message: 'Warning!',
        description: 'e-mail or password incorect!',
    });
};

function* SignInSaga({payload}) {
    try {
        const callBack = yield call(SignInSagaAsync, payload)
        if (callBack.success && callBack.token) {
            localStorage.setItem('token', callBack.token);
            modalSuccess('success');
            Router.push('/messenger/chatroom');
        }else {
            modalSignInWarning('warning')
        }
    } catch (err) {
        console.log(err);
    }
}
const SignInSagaAsync = async (stateForm) => {
    const response = await fetch(`${Config.URL_SERVER}/api/auth/signin`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify(stateForm)
    })
    return await response.json()
}

function* SignUpUserSaga({payload}) {
    try {
        const callBack = yield call(SignUpUserSagaAsync, payload)
        if (callBack.success) {
            modalRegistrationSuccess('success');
        }else {
            modalWarning('warning')
        }
    } catch (err) {
        console.log(err);
    }
}


const SignUpUserSagaAsync = async (stateForm) => {
    const response = await fetch(`${Config.URL_SERVER}/api/auth/signup`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(stateForm)
    })
    return await response.json()
}


function* getUserInformation() {
    try {
        if (localStorage.getItem("token")) {
            const callBack = yield call(getUserInformationAsync)
            if (callBack.success) {
                yield put(signInCandidateInformationSuccess(callBack));
            } else {
                //  localStorage.removeItem("jwt")
                //document.location.href = "/";
            }
        }
    } catch (e) {
        //document.location.href = "/";
        // localStorage.removeItem("jwt")
    }
}


const getUserInformationAsync = async () => {
    let jwt = localStorage.getItem("token")
    const response = await fetch(`${Config.URL_SERVER}/api/auth/getUserInformation`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${jwt}`,
        }
    })
    return await response.json()
}



export default function* rootSaga() {
    yield all([takeEvery(actionTypes.SIGN_UP_USER, SignUpUserSaga)]);
    yield all([takeEvery(actionTypes.SIGN_IN_USER, SignInSaga)]);
    yield all([takeEvery(actionTypes.SIGN_IN_CANDIDATE_INFORMATION, getUserInformation)]);
}
