import {actionTypes} from './action';

export const initState = {
    isLoggedIn: false,
    signUpSuccess : false,
    candidate: {
        information : {
            name :''
        }
    }
};

function reducer(state = initState, action) {
    switch (action.type) {
        case actionTypes.SIGN_UP_USER_SUCCESS:
            return {
                ...state,
                ...{signUpSuccess: true },
            };
        case actionTypes.SIGN_IN_CANDIDATE_INFORMATION_SUCCESS:
            return {
                ...state,
                ...{isLoggedIn: true, candidate: action.payload.candidate},
            };

        default:
            return state;
    }
}

export default reducer;
