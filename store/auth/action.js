export const actionTypes = {
    SIGN_UP_USER: 'SIGN_UP_USER',
    SIGN_UP_USER_SUCCESS: 'SIGN_UP_USER_SUCCESS',
    SIGN_IN_USER: 'SIGN_IN_USER',
    SIGN_IN_USER_SUCCESS: 'SIGN_IN_USER_SUCCESS',
    SIGN_IN_CANDIDATE_INFORMATION: 'SIGN_IN_CANDIDATE_INFORMATION',
    SIGN_IN_CANDIDATE_INFORMATION_SUCCESS: 'SIGN_IN_CANDIDATE_INFORMATION_SUCCESS',
};

export function signUpUser(payload) {
    return { type: actionTypes.SIGN_UP_USER, payload };
}

export function signUpUserSuccess(payload) {
    return { type: actionTypes.SIGN_UP_USER_SUCCESS, payload };
}

export function signInUser(payload) {
    return { type: actionTypes.SIGN_IN_USER, payload };
}

export function signInUserSuccess(payload) {
    return { type: actionTypes.SIGN_IN_USER_SUCCESS, payload };
}

export function signInCandidateInformation(payload) {
    return { type: actionTypes.SIGN_IN_CANDIDATE_INFORMATION, payload };
}

export function signInCandidateInformationSuccess(payload) {
    return { type: actionTypes.SIGN_IN_CANDIDATE_INFORMATION_SUCCESS, payload };
}
