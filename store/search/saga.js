import {all, put, takeEvery, call} from 'redux-saga/effects';
import {Modal, notification} from 'antd';
import {actionTypes, getAllUsersSuccess} from './action';
import Router from "next/router";
import Config from '../../utilities/app-settings'


function* GetAllUsersSaga({payload}) {
    try {
        const callBack = yield call(GetAllUsersSagaAsync, payload)
        if (callBack.success) {
            yield put(getAllUsersSuccess(callBack))
        }
    } catch (err) {
        console.log(err);
    }
}


const GetAllUsersSagaAsync = async (stateForm) => {
    let token = localStorage.getItem("token")
    const response = await fetch(`${Config.URL_SERVER}/api/search/getUsers`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`,
        },
        body: JSON.stringify(stateForm)
    })
    return await response.json()
}


function* SearchUsersSaga({payload}) {
    try {
        const callBack = yield call(SearchUsersSagaAsync, payload)
        if (callBack.success) {
            yield put(getAllUsersSuccess(callBack))
        }
    } catch (err) {
        console.log(err);
    }
}

const SearchUsersSagaAsync = async (stateForm) => {
    let token = localStorage.getItem("token")
    const response = await fetch(`${Config.URL_SERVER}/api/search/getUsers/${stateForm}`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`,
        }
    })
    return await response.json()
}


export default function* rootSaga() {
    yield all([takeEvery(actionTypes.GET_USERS, GetAllUsersSaga)]);
    yield all([takeEvery(actionTypes.SEARCH_USERS, SearchUsersSaga)]);
}
