import {actionTypes} from './action';

export const initState = {
    usersArray: [],
};

function reducer(state = initState, action) {
    switch (action.type) {
        case actionTypes.GET_USERS_SUCCESS:
            return {
                ...state,
                ...{usersArray:  action.payload.users },
            };
        default:
            return state;
    }
}

export default reducer;
