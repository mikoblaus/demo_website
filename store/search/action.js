export const actionTypes = {
    GET_USERS: 'GET_USERS',
    GET_USERS_SUCCESS: 'GET_USERS_SUCCESS',
    SEARCH_USERS: 'SEARCH_USERS',
    SEARCH_USERS_SUCCESS: 'SEARCH_USERS_SUCCESS',
    SEARCH_USERS_FAILED: 'SEARCH_USERS_FAILED',
};

export function getUsers(payload) {
    return { type: actionTypes.GET_USERS, payload };
}

export function getAllUsersSuccess(payload) {
    return { type: actionTypes.GET_USERS_SUCCESS, payload };
}

export function searchUsers(payload) {
    return { type: actionTypes.SEARCH_USERS, payload };
}
export function searchUsersSuccess(payload) {
    return { type: actionTypes.SEARCH_USERS_SUCCESS, payload };
}
export function searchUsersFailed(payload) {
    return { type: actionTypes.SEARCH_USERS_FAILED, payload };
}
