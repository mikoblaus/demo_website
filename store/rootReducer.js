import { combineReducers } from 'redux';
import app from './app/reducer';
import auth from './auth/reducer';
import search from './search/reducer';
import users from './users/reducer';
import chat from './chat/reducer';

export default combineReducers({
    app,
    auth,
    search,
    users,
    chat,
});
