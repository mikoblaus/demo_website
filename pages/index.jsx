import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Container, Row, Col, Button, Form} from "react-bootstrap";

import {validateEmail} from "../utilities/Validate"
import {signUpUser, signInUser} from '../store/auth/action'
import HeaderDefault from '~/components/shared/header/HeaderDefault'

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: true,
            emailValidMessage: false,
            emailSignInValidMessage: false,
            name: '',
            email: '',
            password: '',
            gender: 'male',
            SignInEmail: '',
            SignInPassword: '',
        };
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value
        })
    }

    handleRegistration = event => {
        event.preventDefault()
        if (!this.state.email) {
            this.setState({
                emailValidMessage: true
            })
        }
        else if (this.state.email) {
            if (!validateEmail(this.state.email)) {
                this.setState({
                    emailValidMessage: true
                })
            } else {
                this.props.signUpUser(this.state);
            }
        }
    };

    handleLoginSubmit = event => {
        event.preventDefault()
        if (!this.state.SignInEmail) {
            this.setState({
                emailValidMessage: true
            })
        }
        else if (this.state.SignInEmail) {
            this.props.signInUser(this.state);
        }

    };

    render() {
        return (
            <Container fluid>
                <Row>
                    <Col xs={12} style={{textAlign: 'center', paddingTop: '10px'}}>
                        <HeaderDefault
                            pageName={'Authorization'}
                        />
                    </Col>
                    <Col xs={12} style={{textAlign: 'center', paddingTop: '50px', paddingBottom: '50px'}}>
                        <div className="mb-2">
                            <Button
                                onClick={() => {
                                    this.setState({form: true})
                                }}
                                variant={this.state.form ? 'primary' : 'secondary'} size="lg">
                                Sign In
                            </Button>{' '}
                            <Button
                                onClick={() => {
                                    this.setState({form: false})
                                }}
                                variant={!this.state.form ? 'primary' : 'secondary'} size="lg">
                                Sign up
                            </Button>
                        </div>
                    </Col>
                    <Col xs={2}>
                    </Col>
                    <Col xs={8} style={{backgroundColor: '#CCCCFF', padding: '10px', borderRadius: '10px',}}>
                        {
                            this.state.form
                                ?
                                <>
                                    <Form>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Email address</Form.Label>
                                            <Form.Control type="email"
                                                          value={this.state.SignInEmail}
                                                          onChange={this.handleChange("SignInEmail")}
                                                          placeholder="Enter email"/>
                                            {
                                                this.state.emailSignInValidMessage
                                                    ? <Form.Text className="text-muted">
                                                        Please, enter valid email
                                                    </Form.Text>
                                                    : null
                                            }

                                        </Form.Group>

                                        <Form.Group controlId="formBasicPassword">
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control
                                                value={this.state.SignInPassword}
                                                onChange={this.handleChange("SignInPassword")}
                                                type="password" placeholder="Password"/>
                                        </Form.Group>
                                        <Form.Group controlId="formBasicCheckbox">
                                            <br/>
                                        </Form.Group>
                                        <Button
                                            onClick={this.handleLoginSubmit.bind(this)}
                                            variant="primary" type="button">
                                            Sign In
                                        </Button>
                                    </Form>
                                </>
                                :
                                <>
                                    <Form>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Email address</Form.Label>
                                            <Form.Control
                                                onChange={this.handleChange("email")}
                                                value={this.state.email}
                                                type="email" placeholder="Enter email"/>
                                            {
                                                this.state.emailValidMessage
                                                    ? <Form.Text style={{color:'red'}} className="text-muted">
                                                        Please, enter valid email
                                                    </Form.Text>
                                                    : null
                                            }
                                        </Form.Group>

                                        <Form.Group controlId="formBasicPassword">
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control
                                                onChange={this.handleChange("password")}
                                                value={this.state.password}
                                                type="password" placeholder="Password"/>
                                        </Form.Group>

                                        <Form.Group controlId="formBasicPassword">
                                            <Form.Label>Name</Form.Label>
                                            <Form.Control
                                                onChange={this.handleChange("name")}
                                                value={this.state.name}
                                                type="text" placeholder="Name"/>
                                        </Form.Group>

                                        <Form.Group controlId="exampleForm.ControlSelect1">
                                            <Form.Label>Gender</Form.Label>
                                            <Form.Control
                                                onChange={this.handleChange("gender")}
                                                as="select" style={{backgroundColor: 'white'}}>
                                                <option value='male'>Male</option>
                                                <option value='female'>Female</option>
                                            </Form.Control>
                                        </Form.Group>
                                        <Form.Group controlId="formBasicCheckbox">
                                            <br/>
                                        </Form.Group>
                                        <Button
                                            onClick={this.handleRegistration.bind(this)}
                                            variant="primary" type="button">
                                            Registration
                                        </Button>
                                    </Form>
                                </>
                        }

                    </Col>
                    <Col xs={2}>
                    </Col>
                </Row>
            </Container>

        );
    }
}

const mapStateToProps = (state) => {
    const {
        auth,
    } = state
    return {
        auth,
    }
}

export default connect(mapStateToProps, {
    signUpUser,
    signInUser
})(Index);
