import React, {useEffect, useRef, useState} from "react";
import { useRouter } from 'next/router'

import Button from "@material-ui/core/Button"
import IconButton from "@material-ui/core/IconButton"
import TextField from "@material-ui/core/TextField"
import AssignmentIcon from "@material-ui/icons/Assignment"
import PhoneIcon from "@material-ui/icons/Phone"
import Peer from 'simple-peer'
import {Col, Container, ListGroup, Row, InputGroup, FormControl, Table, FormText} from "react-bootstrap";
import {CopyToClipboard} from "react-copy-to-clipboard"
import io from 'socket.io-client'
import Config from '../../utilities/app-settings'

const socket = io(Config.URL_SERVER)

function VideoChat(props) {
    const router = useRouter()
    const { pid } = router.query
    const [ socketId, setSocketId ] = useState("")
    const [ name, setName ] = useState("")
    const [ callerName, setCallerName ] = useState("")
    const [ chatRoomId, setChatRoomId ] = useState()
    const [ userId, setUserId ] = useState()

    const [ callAccepted, setCallAccepted ] = useState(false)
    const [ receivingCall, setReceivingCall ] = useState(false)
    const [ callEnded, setCallEnded] = useState(false)
    const [ stream, setStream ] = useState()
    const [ caller, setCaller ] = useState("")

    const [ callerSignal, setCallerSignal ] = useState()

    useEffect(() => {
        setName(props.authName);
        setCallerName(props.authName);
        setUserId(props.userID);
        setChatRoomId(pid);
    }, [props])

    const myVideo = useRef()
    const userVideo = useRef()
    const connectionRef= useRef()


    useEffect(()=>{
        navigator.mediaDevices.getUserMedia({ video: true, audio: true }).then((stream) => {
            setStream(stream)
            myVideo.current.srcObject = stream
        })
        socket.on("socketId", (id) => {
            setSocketId(id)
        })
        // if
        // socket.on('callUser')
        if (props.userID) {
            socket.on("reciveCall", (data) => {
                if (props.userID) {
                    if (pid === data.chatRoomId && props.userID !== data.userId) { // After put here
                        console.log("userToCall", data)
                        setReceivingCall(true)
                        setCallerName(data.callerName)
                        setCallerSignal(data.signal)
                        setCaller(data.socketId)
                    }
                }
            })
        }
    },[props])


    const callUser = () => {
        const peer = new Peer({
            initiator: true,
            trickle: false,
            stream: stream
        })
        peer.on("signal", (data) => {
            socket.emit('callUser', {
                socketId,
                name,
                callerName,
                chatRoomId,
                userId,
                signalData : data
            })
        })
        peer.on("stream", (stream) => {

            userVideo.current.srcObject = stream

        })
        socket.on("callAccepted", (signal) => {
            setCallAccepted(true)
            peer.signal(signal)
        })

        connectionRef.current = peer
    }


    const answerCall =() =>  {
        setCallAccepted(true)
        const peer = new Peer({
            initiator: false,
            trickle: false,
            stream: stream
        })
        peer.on("signal", (data) => {
            socket.emit("answerCall", { signal: data, to: caller })
        })
        peer.on("stream", (stream) => {
            userVideo.current.srcObject = stream
        })

        peer.signal(callerSignal)
        connectionRef.current = peer
    }




    const [ me, setMe ] = useState("")


    const [ idToCall, setIdToCall ] = useState("")

    // const [ name, setName ] = useState("")


    useEffect(() => {

/*


        socket.on("callUser", (data) => {
            setReceivingCall(true)
            setCaller(data.from)
            setName(data.name)
            setCallerSignal(data.signal)
        })
        */
    }, [])

    const callUser_4_ = (id) => {
        const peer = new Peer({
            initiator: true,
            trickle: false,
            stream: stream
        })
        peer.on("signal", (data) => {
            socket.emit("callUser", {
                userToCall: id,
                signalData: data,
                from: me,
                name: name
            })
        })
        peer.on("stream", (stream) => {

            userVideo.current.srcObject = stream

        })
        socket.on("callAccepted", (signal) => {
            setCallAccepted(true)
            peer.signal(signal)
        })

        connectionRef.current = peer
    }

    const answerCall_2 =() =>  {
        setCallAccepted(true)
        const peer = new Peer({
            initiator: false,
            trickle: false,
            stream: stream
        })
        peer.on("signal", (data) => {
            console.log("DATA", data)
            socket.emit("answerCall", { signal: data, to: caller })
        })
        peer.on("stream", (stream) => {
            userVideo.current.srcObject = stream
        })

        peer.signal(callerSignal)
        connectionRef.current = peer
    }

    const leaveCall = () => {
        setCallEnded(true)
        connectionRef.current.destroy()
    }

    return (
        <>
            <h5 style={{textAlign: "center", color: '#fff'}}>Chat Room Id: {pid}

            </h5>
            <Col xs={8} style={{backgroundColor:'#404040', padding : '20px'}} >
                <Row>
                    <Col xs={6}>
                        <div className="video">
                            {stream &&  <video playsInline muted ref={myVideo} autoPlay style={{ width: "100%" }} />}
                        </div>
                    </Col>
                    <Col xs={6}>
                        <div className="video">
                            {callAccepted && !callEnded ?
                                <video playsInline ref={userVideo} autoPlay style={{ width: "100%"}} />:
                                null}
                        </div>
                    </Col>
                </Row>
                <div>
                    {receivingCall && !callAccepted ? (
                        <div className="caller">
                            <h1 >{callerName} is calling...</h1>
                            <h5><i>{caller}</i></h5>
                            <Button variant="contained" color="primary" onClick={answerCall}>
                                Answer
                            </Button>
                        </div>
                    ) : null}
                </div>
            </Col>

            <Col xs={4} style={{backgroundColor:'#c9d6ff'}} >
                <div className="call-button">
                    {callAccepted && !callEnded ? (
                        <Button variant="contained" color="secondary" onClick={leaveCall}>
                            End Call
                        </Button>
                    ) : (
                       <>
                           <h3>Click to call</h3>
                           <IconButton color="primary" aria-label="call" onClick={() => callUser(idToCall)}>
                               <PhoneIcon fontSize="large" />
                           </IconButton>
                       </>
                    )}
                    {idToCall}
                </div>
            </Col>



        </>


    )
}

export default VideoChat
