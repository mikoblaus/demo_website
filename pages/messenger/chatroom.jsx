import React, {Component} from 'react';
import {Button, Col, Container, ListGroup, Row, InputGroup, FormControl, Table} from "react-bootstrap";
import {connect} from "react-redux";
import jwt_decode from "jwt-decode";
import {getUsers, searchUsers} from "~/store/search/action";
import {addUser} from "~/store/users/action"
import {getContacts, getMessages, addMessagesSuccess} from "~/store/chat/action"
import Link from 'next/link';
import io from 'socket.io-client'
import Config from '../../utilities/app-settings'
const Socket = io(Config.URL_SERVER)
import videChatImg from '../../assets/images/video-chat.png'
import chatImg from '../../assets/images/chat.png'
import HeaderDefault from "~/components/shared/header/HeaderDefault";
class Chatroom extends Component {

    constructor(props) {
        super(props);
        this.state = {
            form: true,
            value: '',
            chatRoomId: '',
            message: '',
            myId: ''
        };
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value
        })
    }

    componentDidMount() {
        this.props.getUsers()
        this.props.getContacts()

        Socket.on('update_messanger', (data) => {
            if (this.state.chatRoomId === data.chatRoomId) {
                this.props.addMessagesSuccess(data)
            }
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

    }
    sendMessage = (item) => {
        Socket.emit('send_message', {
            item
        })
    }
    chatRooms = (item) => {
        Socket.emit('chat_rooms', {
            item
        })
    }

    handleCheckChatRoom = (chatRoomId) => {
        this.setState({
            chatRoomId: chatRoomId
        })
        this.props.getMessages(chatRoomId)
    }

    handleSendMessage = event => {
        this.setState({
            message: ''
        })
        let item = {
            chatRoomId: this.state.chatRoomId,
            message: this.state.message,
            userId: jwt_decode(localStorage.getItem('token')).userId
        }
        this.sendMessage(item)
    }


    handleSearch = event => {
        event.preventDefault()
        this.props.searchUsers(this.state.value)
    }
    handleAddUser = (id, event) => {
        event.preventDefault()
        let item = {
            user: id
        }
        this.props.addUser(item)
    }


    render() {
        return (
            <Container fluid>
                <Row>
                    <Col xs={12} style={{textAlign: 'center', paddingTop: '10px'}}>
                        <HeaderDefault
                            pageName={'Chat Room'}
                        />
                    </Col>
                    <Col xs={12} style={{textAlign: 'center', paddingTop: '50px', paddingBottom: '50px'}}>
                        <div className="mb-2">
                            <Button
                                onClick={() => {
                                    this.setState({form: true})
                                }}
                                variant={this.state.form ? 'primary' : 'secondary'} size="lg">
                                Messenger
                            </Button>{' '}
                            <Button
                                onClick={() => {
                                    this.setState({form: false})
                                }}
                                variant={!this.state.form ? 'primary' : 'secondary'} size="lg">
                                Search
                            </Button>
                        </div>
                    </Col>
                </Row>
                <Row style={{backgroundColor: '#CCE5FF', padding: '10px', borderRadius: '10px', height: '700px'}}>
                    {
                        this.state.form
                            ? <>
                                <Col xs={4} style={{paddingLeft: '5px'}}>
                                    <div style={{height: '100%', backgroundColor: 'white'}}>
                                        <ListGroup variant="flush">
                                            {
                                                this.props.chat.contactsArray.length > 0
                                                    ?
                                                    <>
                                                        {
                                                            this.props.chat.contactsArray.map((item, key) => {
                                                                return (
                                                                    <ListGroup.Item
                                                                        key={key}
                                                                    >
                                                                        <a href={null}
                                                                           onClick={this.handleCheckChatRoom.bind(this, item._id)}
                                                                        > {item.additionalData.user.information.name}        <img style={{height:'30px'}} src={chatImg}/> </a>


                                                                        <Link href={`/messenger/${item._id}`}>
                                                                            <a className="report"> <img style={{height:'30px'}} src={videChatImg}/></a>
                                                                        </Link>


                                                                    </ListGroup.Item>
                                                                )
                                                            })
                                                        }

                                                    </>
                                                    : <> <ListGroup.Item
                                                        style={{backgroundColor: '#5F88DB'}}> Preloader</ListGroup.Item> </>
                                            }

                                        </ListGroup>
                                    </div>
                                </Col>
                                <Col xs={8}>
                                    <div style={{height: '550px', backgroundColor: 'white',   overflow : 'auto'}}>
                                        {
                                            this.props.chat.messagesStatus
                                                ?
                                                <>
                                                    {
                                                        this.props.chat.messagesStatus
                                                            ?
                                                            <>
                                                                {
                                                                    this.props.chat.messagesArray.length
                                                                        ? <div style={{padding: '10px'}}>
                                                                            {
                                                                                this.props.chat.messagesArray.map((item, key) => {
                                                                                    return (
                                                                                        <>
                                                                                            <div style={{
                                                                                                paddingLeft: '20px',
                                                                                                backgroundColor: '#E5CCFF',
                                                                                                borderRadius: '10px'
                                                                                            }} key={key}>
                                                                                                <p> {item.message}

                                                                                                </p>
                                                                                            </div>

                                                                                        </>
                                                                                    )
                                                                                })
                                                                            }
                                                                        </div>
                                                                        : <> Null Messages</>
                                                                }
                                                            </>
                                                            :
                                                            <>
                                                                Check User
                                                            </>
                                                    }

                                                </>
                                                : <> Check User For chat </>
                                        }


                                    </div>
                                    <div style={{height: '10%', backgroundColor: 'white'}}>
                                        <InputGroup className="mb-3  p-1" style={{}}>
                                            {
                                                this.props.chat.messagesStatus
                                                    ?
                                                    <>
                                                        <FormControl
                                                            value={this.state.message}
                                                            onChange={this.handleChange("message")}
                                                            placeholder="Recipient's username"
                                                            aria-label="Recipient's username"
                                                            aria-describedby="basic-addon2"
                                                        />
                                                        <InputGroup.Append>
                                                            <Button
                                                                onClick={this.handleSendMessage.bind(this)}
                                                                variant="outline-secondary">Send</Button>
                                                        </InputGroup.Append>
                                                    </>
                                                    : null
                                            }

                                        </InputGroup>
                                    </div>
                                </Col>
                            </>
                            :
                            <>
                                <div style={{height: '10%', backgroundColor: 'white'}}>
                                    <InputGroup className="mb-3  p-1" style={{}}>
                                        <FormControl
                                            onChange={this.handleChange("value")}
                                            placeholder="Email or Name"
                                            aria-label=" "
                                            aria-describedby="basic-addon2"
                                        />
                                        <InputGroup.Append>
                                            <Button
                                                type="button"
                                                onClick={this.handleSearch.bind(this)}
                                                variant="outline-secondary">search</Button>
                                        </InputGroup.Append>
                                    </InputGroup>
                                </div>
                                <div>
                                    All users
                                </div>
                                <div style={{height: '90%', backgroundColor: 'white'}}>
                                    <Table striped bordered hover>
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Email</th>
                                            <th>First Name</th>
                                            <th>Gender</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.props.search.usersArray.map((item, key) => {
                                                return (
                                                    <tr key={key}>
                                                        <td>{key + 1}</td>
                                                        <td>{item.email}</td>
                                                        <td>{item.information.name}</td>
                                                        <td>{item.information.gender}</td>
                                                        <td>
                                                            <Button
                                                                onClick={this.handleAddUser.bind(this, item._id)}
                                                            >
                                                                + Add
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </Table>

                                </div>
                            </>

                    }
                </Row>
            </Container>
        )
    }

};
const mapStateToProps = (state) => {
    const {
        auth,
        search,
        chat
    }

        = state
    return {
        auth,
        search,
        chat
    }
}

export default connect(mapStateToProps,
    {
        getUsers,
        getContacts,
        searchUsers,
        addUser,
        getMessages,
        addMessagesSuccess
    }
)(Chatroom);
