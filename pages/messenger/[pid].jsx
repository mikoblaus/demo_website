import React, {Component} from 'react';
import {Button, Col, Container, ListGroup, Row, InputGroup, FormControl, Table} from "react-bootstrap";
import {connect} from "react-redux";
import jwt_decode from "jwt-decode";
import Link from 'next/link';
import io from 'socket.io-client'
import Config from '../../utilities/app-settings'
const Socket = io(Config.URL_SERVER)
import videChatImg from '../../assets/images/video-chat.png'
import WebRTC from './webRTC'

class VideoChat extends Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {

         return (
            <>
                <Container fluid>
                    <Row>
                        <Col xs={12} style={{textAlign: 'center', paddingTop: '10px'}}><h1>Chat Room</h1></Col>
                    </Row>
                    <Row>
                        <WebRTC
                            authName={this.props.auth.candidate.information.name}
                            userID={this.props.auth.candidate._id}
                        />
                    </Row>
                </Container>

            </>
        )
    }

};

const mapStateToProps = (state) => {
    const {
        auth,
        search,
        chat
    }  = state
    return {
        auth,
        search,
        chat
    }
}

export default connect(mapStateToProps,
    {}
)(VideoChat);

